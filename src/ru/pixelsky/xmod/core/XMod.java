package ru.pixelsky.xmod.core;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import ru.pixelsky.xmod.blocks.decoration.BlockColoredBrick;
import ru.pixelsky.xmod.blocks.decoration.BlockColoredFlower;
import ru.pixelsky.xmod.blocks.decoration.BlockJapaneseLantern;
import ru.pixelsky.xmod.blocks.decoration.BlockStool;
import ru.pixelsky.xmod.blocks.resource.BlockCoal;
import ru.pixelsky.xmod.blocks.resource.BlockWheat;
import ru.pixelsky.xmod.blocks.sakura.BlockSakuraFlowers;
import ru.pixelsky.xmod.blocks.sakura.BlockSakuraLeaves;
import ru.pixelsky.xmod.blocks.sakura.BlockSakuraSapling;
import ru.pixelsky.xmod.blocks.sakura.BlockSakuraWood;
import ru.pixelsky.xmod.creative.XModCreativeTab;
import ru.pixelsky.xmod.items.DefaultBlockItem;
import ru.pixelsky.xmod.items.ItemSakuraPetal;
import ru.pixelsky.xmod.items.RandomBox;
import ru.pixelsky.xmod.items.teleport.*;
import ru.pixelsky.xmod.items.xlight.XLightCraftingHandler;
import ru.pixelsky.xmod.lang.LangConstants;
import ru.pixelsky.xmod.lang.NamesRegistry;
import ru.pixelsky.xmod.network.XModPacketHandler;

import java.util.List;


@Mod(modid = "xmod", name = "- by PixelSky", version = "1.1")
@NetworkMod(clientSideRequired = true, serverSideRequired = true, channels = {"XMod|Notify_LOW", "XMod|Notify_HIGH", "XMod|Pickup", "psky-ping"}, packetHandler = XModPacketHandler.class)

public class XMod {
    
    @SidedProxy(clientSide="ru.pixelsky.xmod.core.ClientProxy", serverSide = "ru.pixelsky.xmod.core.CommonProxy")
    
    public static CommonProxy proxy;

    public static final CreativeTabs xModTab = new XModCreativeTab("xMod");

    public static final Block blockWheat = new BlockWheat(2020).setUnlocalizedName("blockWheat");
    public static final Block blockCoal = new BlockCoal(2021).setUnlocalizedName("blockCoal");
    public static final Block blockSakuraLeaves = new BlockSakuraLeaves(2023).setHardness(0.2F).setLightOpacity(1).setStepSound(Block.soundGrassFootstep).setUnlocalizedName("blockSakuraLeaves");
    public static final Block blockSakuraFlowers = new BlockSakuraFlowers(2024).setHardness(0.2F).setLightOpacity(1).setStepSound(Block.soundGrassFootstep).setUnlocalizedName("blockSakuraFlowers");
    public static final Block blockSakuraWood = new BlockSakuraWood(2025).setHardness(2.0F).setResistance(5.0F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("blockSakuraWood");
    public static final Block blockSakuraSapling = new BlockSakuraSapling(2026).setHardness(0.0F).setStepSound(Block.soundGrassFootstep).setUnlocalizedName("blockSakuraSapling");

    public static final Block blockLantern = new BlockJapaneseLantern(2027);
    public static final Block blockFlower = new BlockColoredFlower(2028);
	
    public static final Block blockStoolSingle = new BlockStool(2100, false, Material.wood);
    public static final Block blockStoolDouble = new BlockStool(2101, true, Material.wood);
    public static final Block blockColoredBrick = new BlockColoredBrick(2102, Material.rock);
    public static final BlockTeleportPortal blockTeleportPortal = new BlockTeleportPortal(2103, Material.portal);

    /*
     * Items
     * @params int id
     * Начало id - 3000.
     */
    public static final Item itemSakuraPetal = new ItemSakuraPetal(3000).setUnlocalizedName("sakuraPetal").setCreativeTab(xModTab);
    public static final Item itemLogo = new Item(3001) {
        @Override
        public void addInformation(ItemStack stack, EntityPlayer par2EntityPlayer, List list, boolean par4) {
            list.add("Обменяйте на деньги (" + (stack.stackSize*1000) + ") в админшопе!");
        }
    }
            .setUnlocalizedName("xmod:logo").setCreativeTab(xModTab);
    public static final Item itemRandomBox = new RandomBox(3002).setUnlocalizedName("xmod:randombox").setCreativeTab(xModTab);

    public static final ItemLocationCrystal itemLocationCrystal = (ItemLocationCrystal) new ItemLocationCrystal(3003).setUnlocalizedName("xmod:crystal-pos");
    public static final ItemTeleportCrystal itemTeleportCrystal = (ItemTeleportCrystal) new ItemTeleportCrystal(3004).setUnlocalizedName("xmod:crystal-teleport");
    public static final ItemPortalCrystal itemPortalCrystal = (ItemPortalCrystal) new ItemPortalCrystal(3005).setUnlocalizedName("xmod:crystal-portal");
    public static final ItemTimeCrystal itemTimeCrystal = (ItemTimeCrystal) new ItemTimeCrystal(3006).setUnlocalizedName("xmod:crystal-time");

    public static final Item itemHeadlamp = new Item(3010).setUnlocalizedName("xmod:headlamp").setCreativeTab(xModTab);
    
    @Mod.Instance("xmod")
    public static XMod instance;

    @Mod.Init
    public void load(FMLInitializationEvent event) 
    {
        proxy.registerNotifyRender();
        MinecraftForge.EVENT_BUS.register(this);

        //Регистрация блоков
        GameRegistry.registerBlock(blockWheat);
        GameRegistry.registerBlock(blockCoal);
        GameRegistry.registerBlock(blockSakuraLeaves);
        GameRegistry.registerBlock(blockSakuraFlowers);
        GameRegistry.registerBlock(blockSakuraWood);
        GameRegistry.registerBlock(blockSakuraSapling);
        GameRegistry.registerBlock(blockTeleportPortal);

        GameRegistry.registerBlock(blockLantern, DefaultBlockItem.class, "japanese-lantern");
        GameRegistry.registerBlock(blockFlower, DefaultBlockItem.class, "colored-flower");
        GameRegistry.registerBlock(blockStoolSingle, DefaultBlockItem.class, "stool-single");
        GameRegistry.registerBlock(blockStoolDouble, DefaultBlockItem.class, "stool-double");
        GameRegistry.registerBlock(blockColoredBrick, DefaultBlockItem.class, "colored-brick");

        /*
         * Blocks
         */
        registerStoolsNames();
        registerFlowersNames();
        registerLanternsNames();

        LanguageRegistry.addName(blockWheat, "Блок Пшеницы");
        LanguageRegistry.addName(blockCoal, "Блок Угля");
        LanguageRegistry.addName(blockSakuraLeaves, "Листья Сакуры");
        LanguageRegistry.addName(blockSakuraFlowers, "Цветы Сакуры");
        LanguageRegistry.addName(blockSakuraWood, "Дерево Сакуры");
        LanguageRegistry.addName(blockSakuraSapling, "Росток Сакуры");
        LanguageRegistry.addName(blockTeleportPortal, "Портал");
        /*
         * Items
         */
        LanguageRegistry.addName(itemSakuraPetal, "Лепесток Сакуры");
        LanguageRegistry.addName(itemLogo, "#0044ff1000 скайников");
        LanguageRegistry.addName(itemRandomBox, "Коробка с сюрпризом");
        LanguageRegistry.addName(itemLocationCrystal, "Кристалл позиции");
        LanguageRegistry.addName(itemTeleportCrystal, "Кристалл телепортации");
        LanguageRegistry.addName(itemPortalCrystal, "Кристалл создания портала");
        LanguageRegistry.addName(itemTimeCrystal, "Кристалл времени портала");


        LanguageRegistry.addName(itemHeadlamp, "Фонарь");
        
        /*
         * Recepts.
        */
        GameRegistry.addShapedRecipe(new ItemStack(blockWheat, 1), "XXX","XXX","XXX", 'X', Item.wheat);
        GameRegistry.addShapedRecipe(new ItemStack(blockCoal, 1), "XXX","XXX","XXX",'X', Item.coal);
        GameRegistry.addShapedRecipe(new ItemStack(blockLantern, 1, 0), "YYY", "YCY","YXY", 'X', Block.torchRedstoneActive, 'Y', Item.paper, 'C', new ItemStack(Item.dyePowder, 1, 0));
        GameRegistry.addShapedRecipe(new ItemStack(blockLantern, 1, 1), "YYY", "YCY","YXY", 'X', Block.torchRedstoneActive, 'Y', Item.paper, 'C', new ItemStack(Item.dyePowder, 1, 12));
        GameRegistry.addShapedRecipe(new ItemStack(blockLantern, 1, 2), "YYY", "YCY","YXY", 'X', Block.torchRedstoneActive, 'Y', Item.paper, 'C', new ItemStack(Item.dyePowder, 1, 14));
        GameRegistry.addShapedRecipe(new ItemStack(Item.dyePowder, 1, 11), "Y", 'Y', new ItemStack(blockFlower, 1, 0));
        GameRegistry.addShapedRecipe(new ItemStack(Item.dyePowder, 1, 4), "Y", 'Y', new ItemStack(blockFlower, 1, 1));
        GameRegistry.addShapedRecipe(new ItemStack(Item.dyePowder, 1, 13), "Y", 'Y', new ItemStack(blockFlower, 1, 2));
        GameRegistry.addShapedRecipe(new ItemStack(Item.dyePowder, 1, 9), "Y", 'Y', new ItemStack(blockFlower, 1, 3));

        GameRegistry.addShapelessRecipe(new ItemStack(Item.wheat, 9, 0), blockWheat);
        GameRegistry.addShapelessRecipe(new ItemStack(Item.coal, 9, 0), blockCoal);

        GameRegistry.addShapedRecipe(new ItemStack(itemLocationCrystal, 1), "PRP", "RCR", "PRP", 'P', Item.paper, 'C', Item.compass, 'R', Item.redstone);
        GameRegistry.addShapedRecipe(new ItemStack(itemPortalCrystal, 1), "EKE", "KCK", "EKE", 'E', Item.enderPearl, 'K', Block.blockNetherQuartz, 'C', new ItemStack(itemLocationCrystal, 1, 1));
        GameRegistry.addShapelessRecipe(new ItemStack(itemTeleportCrystal, 1),  new ItemStack(itemLocationCrystal, 1, 1),
                Block.blockNetherQuartz, Item.enderPearl);

        GameRegistry.addShapedRecipe(new ItemStack(itemTimeCrystal, 1, 0), "RGR", "GCG", "RGR", 'R', Item.redstone, 'C', Item.pocketSundial, 'G', Item.ingotGold);
        GameRegistry.addShapedRecipe(new ItemStack(itemTimeCrystal, 1, 1), "DRG", "RCR", "GRD", 'R', Item.redstone, 'C', Item.pocketSundial, 'G', Item.ingotGold, 'D', Item.diamond);
        GameRegistry.addShapedRecipe(new ItemStack(itemTimeCrystal, 1, 2), "DDD", "DCD", "DDD", 'C', Item.pocketSundial, 'D', Item.diamond);

        GameRegistry.addShapelessRecipe(new ItemStack(itemHeadlamp, 1, 0), Item.lightStoneDust, Item.lightStoneDust, Item.leather);

        GameRegistry.registerFuelHandler(new XModFuelHandler());
        GameRegistry.registerCraftingHandler(new TeleportCraftingHandler());

        GameRegistry.registerTileEntity(TileEntityPortal.class, "teleportlocation");

        registerSingleStoolRecipes();
        registerDoubleStoolRecipes();
        registerBricksNames();
    }

    @Mod.PostInit
    public void addHeadlampRecipes(FMLPostInitializationEvent event) {
        for (Item item : Item.itemsList) {
            if (item instanceof ItemArmor) {
                ItemArmor armor = (ItemArmor) item;
                if (armor.armorType == 0) {
                    // Шлем
                    GameRegistry.addShapelessRecipe(new ItemStack(armor, 1, 0), armor, new ItemStack(itemHeadlamp, 1, 0));
                }
            }
        }
        GameRegistry.registerCraftingHandler(new XLightCraftingHandler());
    }

    private void registerBricksNames() {
        for (int i = 0; i < 16; i++)
            NamesRegistry.put(blockColoredBrick.blockID, i, LangConstants.colorsMale[i] + " кирпич");
    }

    private void registerStoolsNames() {
        for (int i = 0; i < 16; i++)
            NamesRegistry.put(blockStoolSingle.blockID, i, LangConstants.colorsMale[i] + " табурет");
        for (int i = 0; i < 16; i++)
            NamesRegistry.put(blockStoolDouble.blockID, i, LangConstants.colorsMale[i] + " высокий табурет");
    }

    private void registerFlowersNames() {
        String[] flowersNames = {"Ромашка", "Василек", "Лаванда", "Тюльпан"};
        for (int i = 0; i < flowersNames.length; i++)
            NamesRegistry.put(blockFlower.blockID, i, flowersNames[i]);
    }

    private void registerLanternsNames() {
        String[] lanternsColors = {"Черная", "Голубая", "Оранжевая"};
        for (int i = 0; i < lanternsColors.length; i++)
            NamesRegistry.put(blockLantern.blockID, i, lanternsColors[i] + " лампа");
    }
    
    private void registerSingleStoolRecipes()
    {
        for(int i = 0; i <=16; i++)
        {
            GameRegistry.addShapedRecipe(new ItemStack(blockStoolSingle, 1, i), "YYY", "X X", 'X', Block.woodSingleSlab, 'Y', new ItemStack(Block.cloth, 1, i));
        }
    }
    
    private void registerDoubleStoolRecipes()
    {
        for(int i = 0; i <=16; i++)
        {
            GameRegistry.addShapedRecipe(new ItemStack(blockStoolDouble, 1, i), "YYY", "X X","X X", 'X', Block.woodSingleSlab, 'Y', new ItemStack(Block.cloth, 1, i));
        }
    }
}