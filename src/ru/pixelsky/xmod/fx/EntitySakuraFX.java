/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xmod.fx;

/**
 *
 * @author Asura
 */

import net.minecraft.world.World;

/**
 *
 * @author Smile
 */
 public final class EntitySakuraFX extends BaseEntityFX
{
     public EntitySakuraFX(World par1World, double par2, double par4, double par6, double par8, double par10, double par12)
    {
        this(par1World, par2, par4, par6, par8, par10, par12, 1.0F);
    }
    public EntitySakuraFX(World world, double d, double d1, double d2, double d3, double d4, double d5, float par14)
    {
        super(world, d, d1, d2, d3, d4, d5);
        this.particleGravity = 0.013F;

        // Set random texture
        this.setParticleTextureIndex(this.rand.nextInt(3));

        if ((d4 == 0.0D) && ((d3 != 0.0D) || (d5 != 0.0D)))
        {
            this.motionX = d3;
            this.motionY = d4;
            this.motionZ = d5; 
        }
        this.motionX *= -0.3D;
        this.motionY = Math.abs(this.motionY);
        this.motionY *= -0.1D;
        this.motionZ *= -0.3D;
        this.particleMaxAge = (int)(250.0D / (Math.random() * 0.8D + 0.2D));
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;

        if (this.particleAge++ >= this.particleMaxAge)
        {
            this.setDead();
        }
        this.motionY -= 0.04D *this.particleGravity;
        moveEntity(this.motionX, this.motionY, this.motionZ);
        this.motionX *= 0.9899999785423279D;
        this.motionY *= 0.9599999785423279D; 
        this.motionZ *= 0.9899999785423279D; 
    }
 }
