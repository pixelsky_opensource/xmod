/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xmod.fx;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.RenderEngine;

/**
 *
 * @author Asura
 */
public class EntityFXManager {
    
    private static Minecraft mc = Minecraft.getMinecraft();
    private static WorldClient theWorld = mc.theWorld;
    
    public static EntityFX spawnParticle(String particleName, double par2, double par4, double par6, double par8, double par10, double par12)
    {
        if (mc != null && mc.renderViewEntity != null && mc.effectRenderer != null)
        {
            int i = mc.gameSettings.particleSetting;

            if (i == 1 && theWorld.rand.nextInt(3) == 0)
            {
                i = 2;
            }    
            
            double d6 = mc.renderViewEntity.posX - par2;
            double d7 = mc.renderViewEntity.posY - par4;
            double d8 = mc.renderViewEntity.posZ - par6;
            EntityFX entityfx = null;
            
            double d9 = 16.0D;

                if (d6 * d6 + d7 * d7 + d8 * d8 > d9 * d9)
                {
                    return null;
                }
                else if (i > 1)
                {
                    return null;
                }
                else
                {
                    if (particleName.equals("sakura"))
                    {
                        entityfx = new EntitySakuraFX(theWorld,  par2, par4, par6, par8, par10, par12);
                    }
                }

            Minecraft.getMinecraft().effectRenderer.addEffect(entityfx);
            return (EntityFX) entityfx;
        }
      return null;
    }
}
    
