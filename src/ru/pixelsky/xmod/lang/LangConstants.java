package ru.pixelsky.xmod.lang;

/**
 * Used for storing widely-used constants such as color names
 */
public class LangConstants {
    public static final String[] colorsMale = new String[] {
            "Белый", "Оранжевый", "Пурпурный", "Светло-синий ", "Желтый", "Лаймовый", "Розовый", "Серый", "Светло-серый", "Голубой", "Фиолетовый",
            "Синий", "Коричневый", "Зеленый", "Красный", "Черный"
    };
    public static final String[] colorsFemale = new String[] {
            "Белая", "Оранжевая", "Пурпурная", "Светло-синия", "Желтая", "Лаймовая", "Розовая", "Серая", "Светло-серая", "Голубая", "Фиолетовая",
            "Синяя", "Коричневая", "Зеленая", "Красная", "Черная"
    };
}
