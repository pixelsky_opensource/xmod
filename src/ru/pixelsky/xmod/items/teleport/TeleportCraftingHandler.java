package ru.pixelsky.xmod.items.teleport;

import cpw.mods.fml.common.ICraftingHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.pixelsky.xmod.core.XMod;

public class TeleportCraftingHandler implements ICraftingHandler {
    @Override
    public void onCrafting(EntityPlayer player, ItemStack item, IInventory craftMatrix) {
        if (item.itemID == XMod.itemTeleportCrystal.itemID || item.itemID == XMod.itemPortalCrystal.itemID) {
            ItemStack locationCrystal = getLocationCrystal(craftMatrix);

            TeleportPlace p = XMod.itemLocationCrystal.getTeleportPlace(locationCrystal);
            ((ItemLocationed)Item.itemsList[item.itemID]).setTeleportPlace(item, p);
        }
    }

    @Override
    public void onSmelting(EntityPlayer player, ItemStack item) {

    }

    private ItemStack getLocationCrystal(IInventory inventory) {
        for (int i = 0; i < inventory.getSizeInventory(); i++) {
            ItemStack slot = inventory.getStackInSlot(i);
            if (slot != null && slot.itemID == XMod.itemLocationCrystal.itemID) {
                return slot;
            }
        }
        return null;
    }
}
