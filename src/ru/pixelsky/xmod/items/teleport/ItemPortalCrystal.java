package ru.pixelsky.xmod.items.teleport;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import ru.pixelsky.xmod.core.XMod;

public class ItemPortalCrystal extends ItemLocationed {
    public ItemPortalCrystal(int id) {
        super(id);
    }

    @Override
    public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int par7, float par8, float par9, float par10) {
        if (world.isRemote)
            return false;

        TeleportPlace p = getTeleportPlace(itemStack);
        if (spawnPortal(world, player, p, x, y + 1, z)) {
            itemStack.stackSize --;
        }

        return true;
    }

    private boolean spawnPortal(World world, EntityPlayer player, TeleportPlace place, int px, int py, int pz) {
        world.setBlock(px, py, pz, XMod.blockTeleportPortal.blockID);
        TileEntityPortal tileEntity = new TileEntityPortal();
        tileEntity.setPlace(place);
        world.setBlockTileEntity(px, py, pz, tileEntity);

        return true;
    }
}
