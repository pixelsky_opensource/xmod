package ru.pixelsky.xmod.items.teleport;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import ru.pixelsky.xmod.core.XMod;
import ru.pixelsky.xmod.lang.TimeFormat;

import java.util.List;

public class ItemTimeCrystal extends Item {
    public ItemTimeCrystal(int id) {
        super(id);
        setCreativeTab(XMod.xModTab);
    }

    @Override
    public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int par7, float par8, float par9, float par10) {
        if (world.isRemote)
            return false;

        int blockId = world.getBlockId(x, y, z);
        if (blockId == XMod.blockTeleportPortal.blockID) {
            TileEntityPortal tileEntity = XMod.blockTeleportPortal.getTileEntity(world, x, y, z);
            if (tileEntity != null) {
                tileEntity.addActiveTime(getAddedTime(itemStack.getItemDamage()));
                world.setBlockTileEntity(x, y, z, tileEntity);
                return true;
            }
        }
        return false;
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer par2EntityPlayer, List list, boolean par4) {
        list.add("+ " + TimeFormat.formatTime(getAddedTime(stack.getItemDamage())));
    }

    public long getAddedTime(int meta) {
        switch (meta) {
            case 0:
                return 20L * 60 * 60;
            case 1:
                return 20L * 60 * 60 * 24;
            case 2:
                return 20L * 60 * 60 * 24 * 7;
            default:
                return 0;
        }
    }

    @Override
    public void getSubItems(int id, CreativeTabs tab, List list) {
        for (int i = 0; i <= 2; i++)
            list.add(new ItemStack(this, 0, i));
    }
}
