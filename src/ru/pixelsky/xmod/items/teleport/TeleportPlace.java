package ru.pixelsky.xmod.items.teleport;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;

public class TeleportPlace {
    private int dimension;
    private int x, y, z;

    public TeleportPlace() {
    }

    public TeleportPlace(int dimension, int x, int y, int z) {
        this.dimension = dimension;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getDimension() {
        return dimension;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public void writeToNBT(NBTTagCompound compound) {
        compound.setInteger("tx", x);
        compound.setInteger("ty", y);
        compound.setInteger("tz", z);
        compound.setInteger("tdim", dimension);
    }

    public void readFromNBT(NBTTagCompound compound) {
        x = compound.getInteger("tx");
        y = compound.getInteger("ty");
        z = compound.getInteger("tz");
        dimension = compound.getInteger("tdim");
    }

    public void teleportPlayer(EntityPlayer player) {
        TeleportPlace p = this;
        if (player.dimension != p.getDimension())
            player.travelToDimension(p.getDimension());
        player.setPositionAndUpdate(p.getX(), p.getY(), p.getZ());
    }

    @Override
    public String toString() {
        return "TeleportPlace{" +
                "dimension=" + dimension +
                ", x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
