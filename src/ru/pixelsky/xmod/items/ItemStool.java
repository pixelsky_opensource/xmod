package ru.pixelsky.xmod.items;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemStool extends ItemBlock {
    public ItemStool(int id) {
        super(id);
        this.setHasSubtypes(true);
    }

    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    public String getUnlocalizedName(ItemStack itemStack)
    {
        return super.getUnlocalizedName() + "." + itemStack.getItemDamage();
    }
}
