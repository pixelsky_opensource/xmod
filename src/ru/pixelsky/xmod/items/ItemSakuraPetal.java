/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xmod.items;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import ru.pixelsky.xmod.core.ClientProxy;
import ru.pixelsky.xmod.core.XMod;

/**
 *
 * @author Asura
 */
public class ItemSakuraPetal extends Item
{
    public ItemSakuraPetal(int i)
    {
        super(i);
        this.maxStackSize = 64;
        this.setCreativeTab(CreativeTabs.tabMaterials);
    }
    
    @Override
    public void registerIcons(IconRegister par1IconRegister) 
    {
        this.itemIcon = par1IconRegister.registerIcon("xmod:sakurapetal1");
    }

    @Override
    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player) {
        return itemStack;
    }
}
