package ru.pixelsky.xmod.items.xlight;

import cpw.mods.fml.common.ICraftingHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import ru.pixelsky.xmod.core.XMod;

public class XLightCraftingHandler implements ICraftingHandler {
    @Override
    public void onCrafting(EntityPlayer player, ItemStack item, IInventory craftMatrix) {
        if (item.getItem() instanceof ItemArmor && containsHeadlamp(craftMatrix)) {
            NBTTagCompound compound = item.getTagCompound();
            if (compound == null) {
                compound = new NBTTagCompound();
                item.setTagCompound(compound);
            }
            NBTTagCompound xlight = new NBTTagCompound();
            xlight.setInteger("level", 15);
            xlight.setInteger("duration", 300 * 20);
            compound.setCompoundTag("xlight", xlight);
        }
    }

    @Override
    public void onSmelting(EntityPlayer player, ItemStack item) {

    }

    private boolean containsHeadlamp(IInventory inv) {
        int size = inv.getSizeInventory();
        for (int i = 0; i < size; i++) {
            ItemStack itemStack = inv.getStackInSlot(i);
            if (itemStack != null && itemStack.itemID == XMod.itemHeadlamp.itemID)
                return true;
        }
        return false;
    }
}
