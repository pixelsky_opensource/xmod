package ru.pixelsky.xmod.blocks;

public class BlockConstants {
    public static final int SIDE_TOP = 1;
    public static final int SIDE_BOTTOM = 0;
    public static final int SIDE_LEFT = 5;
    public static final int SIDE_RIGHT = 4;
    public static final int SIDE_FRONT = 3;
    public static final int SIDE_BACK = 2;
}
