package ru.pixelsky.xmod.blocks.resource;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;

import java.util.Random;

import net.minecraft.item.Item;
import net.minecraft.util.Icon;

/**
 * Used for wheat storing
 */
public class BlockWheat extends Block 
{
    private Icon sideIcon;
    private Icon topIcon;
    private Icon frontIcon;
    private Icon bottomIcon;

    public BlockWheat(int par1) {
        super(par1, Material.grass);
        this.setCreativeTab(CreativeTabs.tabBlock);
    }

    public void registerIcons(IconRegister par1IconRegister) {
        topIcon = par1IconRegister.registerIcon("xmod:resource/wheat_top");
        bottomIcon = par1IconRegister.registerIcon("xmod:resource/wheat_bottom");
        frontIcon = par1IconRegister.registerIcon("xmod:resource/wheat_front");
        sideIcon = par1IconRegister.registerIcon("xmod:resource/wheat_side");
    }

    public Icon getIcon(int side, int metadata) {
        if (side == 0)
            return bottomIcon;
        else if (side == 1)
            return topIcon;
        else if ((side == 2 && metadata == 2) || (side == 5 && metadata == 3) || (side == 3 && metadata == 0) || (side == 4 && metadata == 1))
            return frontIcon;
        else
            return sideIcon;
    }

    @Override
    public int quantityDropped(Random random)
    {
        return 9;
    }

    @Override
    public int idDropped(int id, Random random, int fortune)
    {
        return Item.wheat.itemID;
    }
}

