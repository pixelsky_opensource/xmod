package ru.pixelsky.xmod.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;

public class BlockPartiallyTransparent extends Block {

    public BlockPartiallyTransparent(int par1, Material par2Material) {
        super(par1, par2Material);
    }

    @SideOnly(Side.CLIENT)
    public int getRenderBlockPass()
    {
        return 0;
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    @SideOnly(Side.CLIENT)
    public boolean renderAsNormalBlock()
    {
        return false;
    }


    @Override
    public boolean isBlockSolidOnSide(World world, int x, int y, int z,
                                      ForgeDirection side) {
        return false;
    }
}