package ru.pixelsky.xmod.blocks.decoration;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import ru.pixelsky.xmod.blocks.BlockConstants;
import ru.pixelsky.xmod.blocks.BlockPartiallyTransparent;
import ru.pixelsky.xmod.core.XMod;

import java.util.List;
import java.util.Random;

/**
 * Stool block or half-block
 */
public class BlockStool extends BlockPartiallyTransparent {

    protected Icon[] topIcons = new Icon[16];
    protected Icon sideIcon;
    protected boolean isDouble;

    public BlockStool(int id, boolean isDouble, Material mat) {
        super(id, mat);
        this.setCreativeTab(XMod.xModTab);
        this.setHardness(5.0f);
        this.isDouble = isDouble;
        if (!isDouble)
            setBlockBounds(0f, 0f, 0f, 1.0f, 0.5f, 1.0f);

        setUnlocalizedName(isDouble ? "doublestool" : "stool");

    }


    @SideOnly(Side.CLIENT)
    @Override
    public Icon getIcon(int side, int metadata) {
        switch (side) {
            case BlockConstants.SIDE_TOP:
                return topIcons[metadata % topIcons.length];
            default:
                return sideIcon;
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister iconRegister) {
        for (int i = 0; i < topIcons.length; i++)
            topIcons[i] = iconRegister.registerIcon("xmod:stools/stool_" + i);
        sideIcon = iconRegister.registerIcon("xmod:stools/stool_side");
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public int idDropped(int par1, Random random, int par3) {
        return this.blockID;
    }

    @Override
    public int damageDropped(int metadata) {
        return metadata;
    }

    @Override
    public void getSubBlocks(int id, CreativeTabs tabs, List list) {
        for (int i = 0; i < 16; i++)
            list.add(new ItemStack(id, 1, i));
    }
}