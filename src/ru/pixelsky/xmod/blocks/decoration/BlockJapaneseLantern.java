package ru.pixelsky.xmod.blocks.decoration;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import ru.pixelsky.xmod.core.XMod;

import java.util.List;

/**
 * Lantern in japanese style
 */
public class BlockJapaneseLantern extends Block {
    private static final int LANTERNS_COUNT = 3;
    private Icon[] icons;

    public BlockJapaneseLantern(int par1)
    {
        super(par1, Material.glass);
        this.setCreativeTab(CreativeTabs.tabDecorations);
        setLightValue(1);
        setUnlocalizedName("japaneselantern");
    }

    @Override
    public void registerIcons(IconRegister iconRegister)
    {
        icons = new Icon[LANTERNS_COUNT];
        icons[0] = iconRegister.registerIcon("xmod:lanterns/black");
        icons[1] = iconRegister.registerIcon("xmod:lanterns/blue");
        icons[2] = iconRegister.registerIcon("xmod:lanterns/orange");

    }

    @Override
    public Icon getIcon(int side, int metadata) {
        return icons[metadata];
    }

    @Override
    public void getSubBlocks(int id, CreativeTabs tabs, List list) {
        for (int i = 0; i < LANTERNS_COUNT; i++)
            list.add(new ItemStack(id, 1, i));
    }

    @Override
    public int getRenderType()
    {
        return 1;
    }

    @Override
    public boolean isOpaqueCube()
    {
        return false;
    }

    public int quantityDropped(int par1)
    {
        return 1;
    }

    public int idDropped(int par1, int par3)
    {
        return blockID;
    }
    
    @Override
    public int damageDropped(int metadata) {
        return metadata;
    }
    
    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int i)
    {
        return null;
    }
}
