/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xmod.blocks.sakura;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.world.ColorizerFoliage;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import ru.pixelsky.xmod.core.XMod;

/**
 *
 */
public class BlockSakuraLeaves extends Block {
    private Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+4"));
    private long lastDayUpdate = 0;

    public BlockSakuraLeaves(int i) {
	super(i, Material.leaves);
        this.setCreativeTab(CreativeTabs.tabDecorations);
    }
    
    @Override
    public boolean renderAsNormalBlock() {
	    return false;
    }
    
    @Override
    public int idDropped(int par1, Random random, int zero)
    {
        // TODO: dropping leaves is not the best idea, may be drop saplings?
        return XMod.blockSakuraLeaves.blockID;
    }
    
    @Override
    public boolean isOpaqueCube() {
	    return false;
    }
	
    @Override
    public void registerIcons(IconRegister par1IconRegister) 
    {
        this.blockIcon = par1IconRegister.registerIcon("xmod:BlockSakuraFlowers");
    }
    
    @Override
    public int getRenderColor(int i)
    {
    	return ColorizerFoliage.getFoliageColorPine();
    }
    
    @Override
    public int colorMultiplier(IBlockAccess iblockaccess, int i, int j, int k) 
    {
    	return ColorizerFoliage.getFoliageColorPine();
    }

    @Override
    public boolean getTickRandomly() {
        return true;
    }

    @Override
    public void updateTick(World world, int x, int y, int z, Random random) {
        if (System.currentTimeMillis() - lastDayUpdate > 10 * 60 * 1000) {
            lastDayUpdate = System.currentTimeMillis();
            calendar.setTime(new Date(lastDayUpdate));
        }
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && random.nextInt(5) == 0) {
            world.setBlock(x, y, z, XMod.blockSakuraFlowers.blockID);
        }
    }
}
