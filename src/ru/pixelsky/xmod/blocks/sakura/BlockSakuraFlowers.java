package ru.pixelsky.xmod.blocks.sakura;

import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.world.World;
import ru.pixelsky.xmod.core.XMod;
import ru.pixelsky.xmod.fx.EntityFXManager;


/**
 * Sometimes sakura leaves transform into flowers
 * Flowers drop theyself or petals
 */
public class BlockSakuraFlowers extends Block 
{

    public BlockSakuraFlowers(int i)
    {
	    super(i, Material.leaves);
        this.setCreativeTab(CreativeTabs.tabDecorations);
    }
    
    @Override
    public void registerIcons(IconRegister par1IconRegister) 
    {
        this.blockIcon = par1IconRegister.registerIcon("xmod:BlockSakuraFlowers");
    }
    
    @Override
    public boolean isOpaqueCube() 
    {
        return false;
    }
    
    public int idDropped(int par1, Random random, int zero)
    {
        return random.nextInt(10) > 0 ? XMod.itemSakuraPetal.itemID : XMod.blockSakuraFlowers.blockID;
    }
    
    @Override    
    public void randomDisplayTick(World world, int i, int j, int k, Random random)
    {
        if(random.nextInt(32) != 0) return;
        if(!world.isAirBlock(i, j - 1, k)) return;
        
        double d = (float)i + random.nextFloat();
	    double d1 = (float)j + 0.0F;
	    double d2 = (float)k + random.nextFloat();
	    EntityFXManager.spawnParticle("sakura", d, d1, d2, 0.0D, 0.0D, 0.0D);
    }
}
