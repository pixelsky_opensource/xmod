/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xmod.blocks.sakura;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.util.Icon;
import ru.pixelsky.xmod.core.XMod;


/**
 * Wood used in sakura tree
 */
public class BlockSakuraWood extends Block {
    
    private Icon sideIcon;
    private Icon topIcon;
    private Icon frontIcon;
    private Icon bottomIcon;
    
    public BlockSakuraWood(int par1) 
    {
        super(par1, Material.wood);
        this.setCreativeTab(CreativeTabs.tabBlock);
    }
    
    @Override
    public void registerIcons(IconRegister par1IconRegister)
    {
        topIcon = par1IconRegister.registerIcon("xmod:BlockSakuraLog");
        bottomIcon = par1IconRegister.registerIcon("xmod:BlockSakuraLog");
        frontIcon = par1IconRegister.registerIcon("xmod:BlockSakuraLogSide");
        sideIcon = par1IconRegister.registerIcon("xmod:BlockSakuraLogSide");
    }
    
    public Icon getIcon(int side, int metadata) {
        if (side == 0)
            return bottomIcon;
        else if (side == 1)
            return topIcon;
        else if ((side == 2 && metadata == 2) || (side == 5 && metadata == 3) || (side == 3 && metadata == 0) || (side == 4 && metadata == 1))
            return frontIcon;
        else
            return sideIcon;
    }
    
    public int quantityDropped() 
    {
        return 1;
    }

    public int idDropped(int par1, int par3) 
    {
        return XMod.blockSakuraWood.blockID;
    }
}