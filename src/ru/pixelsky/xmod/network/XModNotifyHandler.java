/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xmod.network;

import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import ru.pixelsky.xmod.core.XMod;

import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.charset.Charset;


/**
 *
 * @author Smile
 */
public class XModNotifyHandler implements IPacketHandler
{
    private final String NOTIFY_LOW = "XMod|Notify_LOW";
    private final String NOTIFY_HIGHT = "XMod|Notify_HIGH";
    private final String NOTIFY_PICKUP = "XMod|Pickup";

    private static final Charset charset = Charset.forName("UTF-8");

    @Override
    public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player) 
    {
        if(packet.channel.equalsIgnoreCase(NOTIFY_LOW))
        {
            String message = new String(packet.data, charset);
            if(message.isEmpty())
            {
                XMod.proxy.addLowMessageToArray("Ошибка, пожалуйста сообщите администратору.");
                return;
            }
            XMod.proxy.addLowMessageToArray(message);
        }
        else if(packet.channel.equalsIgnoreCase(NOTIFY_HIGHT))
        {
            String message = new String(packet.data, charset);
            if(message.isEmpty())
            {
                XMod.proxy.addLowMessageToArray("Ошибка, пожалуйста сообщите администратору.");
                return;
            }
            XMod.proxy.addHightMessageToArray(message);
        } else if(packet.channel.equalsIgnoreCase(NOTIFY_PICKUP))
        {
            try {
                DataInput input = new DataInputStream(new ByteArrayInputStream(packet.data));
                NBTTagCompound compound = (NBTTagCompound) NBTBase.readNamedTag(input);

                ItemStack itemStack = new ItemStack(1, 1, 0);
                itemStack.readFromNBT(compound);

                char rarityColor = Character.forDigit(itemStack.getRarity().rarityColor, 16);

                XMod.proxy.addLowMessageToArray("+ §" + rarityColor + itemStack.getDisplayName() + getDamageString(itemStack));
            } catch (IOException e) {
                /* Reading error */
                e.printStackTrace();
                XMod.proxy.addLowMessageToArray("Ошибка, пожалуйста сообщите администратору.");
            }
        }
    }


    public static String getDamageString(ItemStack is) {
        if (is == null || is.getMaxDamage() <= 0)
            return "";

        int percent = ((is.getMaxDamage() - is.getItemDamage()) * 100 / is.getMaxDamage());

        String damageString = (is.getMaxDamage() - is.getItemDamage()) + "§r/" + "§2" + (is.getMaxDamage()) + "§r";

        if (percent <= 25)
        {
            return "§4" + damageString;
        }
        else if (percent <= 50)
        {
            return "§6" + damageString;
        }
        else
        {
            return "§2" + damageString;
        }
    }
    
}
